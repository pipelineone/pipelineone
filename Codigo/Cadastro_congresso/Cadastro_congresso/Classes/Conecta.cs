﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadastro_congresso.Classes
{
    public class Conecta
    {
        //Definir a String de Conexão
        static String strConexao = @"SERVER=127.0.0.1;DATABASE=cadcongress;UID=root;PASSWORD=123456;";
        //criar a Conexão com o DB
        MySqlConnection con = new MySqlConnection(strConexao);

        //executa comendos de Inserção, atualização e Exclusão
        public String ExecutaComandos(String comandoSql)
        {
            try
            {
                //Abre a conexão com o BD
                con.Open();
                //Define o comando SQL
                MySqlCommand cmd = new MySqlCommand(comandoSql, con);
                //Executa o comando SQL
                cmd.ExecuteNonQuery();
                return "Ok";
            }
            catch (Exception erro)
            {
                return erro.Message;
            }
            finally
            {
                con.Close(); //Fecha a Conexão com o BD
            }

        }
    }
}