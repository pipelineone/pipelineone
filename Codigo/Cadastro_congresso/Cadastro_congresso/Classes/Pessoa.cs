﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadastro_congresso.Classes
{
    public class Pessoa
    {
        public String Nome { get; set; }
        public String Endereco { get; set; }
        public String Telefone { get; set; }
        public String Email { get; set; }
        public String Local_trabalho { get; set; }
        public String Num_card { get; set; }
        public DateTime Data_Vencimento { get; set; }
        public String Bandeira { get; set; }
        
    }
}