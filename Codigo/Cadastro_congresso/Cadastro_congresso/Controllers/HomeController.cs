﻿using Cadastro_congresso.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cadastro_congresso.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //Pessoa pessoa = new Pessoa();

            Conecta conecta = new Conecta();
            
            return View();
        }

        public ActionResult Quest()
        {
            

            return View();
        }

        [HttpPost]
        public ActionResult PagCad(Conecta conecta)
        {
            try
            {
                var teste = conecta;

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: Home/Quest
        [HttpPost]
        public ActionResult Quest(Pessoa pessoa)
        {
            try
            {
                var teste = pessoa;

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}