﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PagCad.aspx.cs" Inherits="Cadastro_congresso.PagCad" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="style.css" rel="stylesheet" />
    <title></title>
    <style type="text/css">
        .row {
            width: 951px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <div>
            <h1>Pagina de Cadastro</h1>

            <fieldset>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1">
                            <asp:Label ID="lblNome" runat="server" Text="Nome: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="login" runat="server" Text="Login: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtLogin" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="senha" runat="server" Text="senha: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtSenha" runat="server"></asp:TextBox>
                        </div>
                        <br />
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/favicon.ico" OnClick="ImageButton1_Click" />
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                        <br />
                        <br />
                        <div class="col-md-12">
                            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" />
                        </div>
                        <br />
                        <div class="col-md-12">
                            <asp:Label ID="lblMsg" runat="server" Text="..."></asp:Label>
                        </div>
                    </div>
                </div>
            </fieldset> 
            <br />
        </div>
    </form>
</body>
</html>