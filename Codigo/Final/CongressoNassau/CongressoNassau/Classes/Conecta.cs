﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace CongressoNassau.Classes
{
    public class Conecta
    {
        //Definir a String de Conexão com o banco de dados
        MySqlConnection con = new MySqlConnection(@"Data Source = 127.0.0.1; port = 3306; Initial Catalog = congresso_nassau; User Id = root; password = '123456'");


        //executa comendos de Inserção, atualização e Exclusão
        public String ExecutaComandos(String comandoSql)
        {
            try
            {
                //Abre a conexão com o BD
                con.Open();
                //Define o comando SQL
                MySqlCommand cmd = new MySqlCommand(comandoSql, con);
                //Executa o comando SQL
                cmd.ExecuteNonQuery();
                return "Ok";
            }
            catch (Exception erro)
            {
                return erro.Message;
            }
            finally
            {
                con.Close(); //Fecha a Conexão com o BD
            }

        }
    }
}