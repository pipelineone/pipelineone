﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CongressoNassau.Pags
{
    public partial class CadOpcoes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPart_Click(object sender, EventArgs e)
        {
            Response.Redirect("CadastroParticipante.aspx");
        }

        protected void btnAval_Click(object sender, EventArgs e)
        {
            Response.Redirect("CadastroAvaliador.aspx");
        }
    }
}