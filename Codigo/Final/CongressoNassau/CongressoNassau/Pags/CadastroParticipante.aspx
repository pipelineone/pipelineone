﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CadastroParticipante.aspx.cs" Inherits="CongressoNassau.Pags.CadastroParticipante" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
        <div style="text-align: center; background-color: #99CCFF">
            <h1>Pagina de Cadastro</h1>

            <fieldset style="width: 313px">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1">
                            <asp:Label ID="lblPrimeiro" runat="server" Text="Primeiro Nome: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtPrimeiro" runat="server" Width="134px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblUltimo" runat="server" Text="Ultimo Nome: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtUltimo" runat="server" Width="134px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblCPF" runat="server" Text="CPF: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtCPF" runat="server" Width="135px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblEndereco" runat="server" Text="Endereco: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtEndereco" runat="server" Width="135px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblTel" runat="server" Text="Telefone: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtTel" runat="server" Width="139px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtEmail" runat="server" Width="140px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblLoc" runat="server" Text="Local de Trabalho: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtLoc" runat="server" Width="141px"></asp:TextBox>
                        </div>
                        
                        <div class="col-md-12">
                            <asp:Label ID="lblBandeira" runat="server" Text="Cartão de Credito:"></asp:Label>
                            <br />
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="63px" ImageUrl="~/Imagens/master.png" Width="78px" OnClick="ImageButton1_Click" />
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagens/visa.png" Height="36px" Width="78px" OnClick="ImageButton2_Click"/>
                            <br />
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblNumcard" runat="server" Text="Numero do Cartão: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtNumCard" runat="server" Width="140px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblVencimento" runat="server" Text="Data de Vencimento: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtVenc" runat="server" Width="141px"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblSenha" runat="server" Text="Senha: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtSenha" TextMode="Password" runat="server" Width="139px"></asp:TextBox>
                        </div>
                        <br />
                        <div class="col-md-12">
                            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" Height="42px" Width="145px" />
                        </div>
                        <br />
                        <div class="col-md-12">
                            <asp:Label ID="lblMsg" runat="server" Text="..."></asp:Label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <br />
        </div>
    </form>
</body>
</html>
