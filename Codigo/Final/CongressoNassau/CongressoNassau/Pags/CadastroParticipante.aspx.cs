﻿using System;
using CongressoNassau.Classes;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace CongressoNassau.Pags
{
    public partial class CadastroParticipante : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            lblNumcard.Text = "-Mastercard- Numero do Cartão:";
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            lblNumcard.Text = "-Visa- Numero do Cartão:";
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            string primeiro_nome = txtPrimeiro.Text;
            string ultimo_nome = txtUltimo.Text;
            string cpf = txtCPF.Text;
            string endereco = txtEndereco.Text;
            string telefone = txtTel.Text;
            string email = txtEmail.Text;
            string local_trabalho = txtLoc.Text;
            string num_card = txtNumCard.Text;
            string data_venc = txtVenc.Text;
            string senha = txtSenha.Text;


            Conecta con = new Conecta();

            String cmd = @"INSERT INTO participante(primeiro_nome,ultimo_nome,cpf,endereco,telefone,email,local_trabalho,num_card,data_venc,senha)
            VALUES('" + primeiro_nome + "','" + ultimo_nome + "','" + cpf + "','" + endereco + "','" + telefone + "','" + email + "','" + local_trabalho + "','" + num_card + "','" + data_venc + "','" + senha + "',)";


            //Executa o comando SQL Insert Into
            string resultado = con.ExecutaComandos(cmd);

            if (resultado.Equals("Ok"))
            {
                lblMsg.Text = "Cadastrado com Sucesso!";
                //this.BuscaFuncionarios();
            }
            else
            {
                lblMsg.Text = resultado;
            }
            Response.Redirect("ConfirmaParticipante.aspx");
        }
    }
}