﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnviaArtigo.aspx.cs" Inherits="CongressoNassau.Pags.EnviaArtigo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #form1 {
            height: 799px;
            text-align: center;
            background-color: #99CCFF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Enviar Artigo</h1>
        <br />
        <div>
        </div>
        <asp:Label ID="lblAutor" runat="server" Text="Autor"></asp:Label>
        <br />
        <asp:TextBox ID="txtAutor" runat="server" Width="219px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="lblDescricao" runat="server" Text="Descrição"></asp:Label>
        <br />
        <asp:TextBox ID="txtDescricao" runat="server" Width="218px" Height="73px" TextMode="MultiLine"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="lblArquivo" runat="server" Text="Artigo"></asp:Label>
        <br />
        <asp:FileUpload ID="fuArquivo" runat="server" />
        <br />
        <br />
        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" Width="224px" OnClick="btnEnviar_Click" />
        <br />
        <asp:Label ID="lblMsg" runat="server" Text="..."></asp:Label>
        <br />
        <asp:HyperLink ID="hpPainel" runat="server" NavigateUrl="~/PagsASPX/PainelParticipante.aspx">...</asp:HyperLink>
        
    </form>
</body>
</html>
