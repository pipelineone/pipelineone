﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginParticipante.aspx.cs" Inherits="CongressoNassau.Pags.LoginParticipante" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 149px;
        }
    </style>
</head>
<body style="text-align: center; height: 619px; background-color: #99CCFF">
    <form id="form1" runat="server">
        <div style="height: 435px">
            <h1>&nbsp;</h1>
            <h1>Pagina de Login</h1>
            <br />
            <br />
            <table align="center">
                <tr>
                    <td>Login:</td>
                    <td class="auto-style1"><asp:TextBox ID="txtCPF" runat="server" ForeColor="Gray" Width="141px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td class="auto-style1"><asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="141px"></asp:TextBox></td>
                </tr>
                
                <tr>
                    <td colspan="2" align="center">
                        <br />
                        *Entre com o CPF e senha<br />
                        <br />
                        <asp:Button ID="btnEntrar" runat="server" Text="Login" Font-Bold="True" OnClick="btnEntrar_Click" Height="32px" Width="197px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lbl1" runat="server" Text="..."></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
