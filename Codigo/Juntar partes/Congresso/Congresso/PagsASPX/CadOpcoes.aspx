﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CadOpcoes.aspx.cs" Inherits="Congresso.PagsASPX.CadOpcoes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <h3>Escolha a opção de cadastro</h3>

            <asp:HyperLink ID="hlParticipante" runat="server" NavigateUrl="~/PagsASPX/CadastroParticipante.aspx">Participante</asp:HyperLink>
            <br />
            <asp:HyperLink ID="hlAvaliador" runat="server" NavigateUrl="~/PagsASPX/CadastroAvaliador.aspx">Avaliador</asp:HyperLink>
            <br />
        </div>
    </form>
</body>
</html>
