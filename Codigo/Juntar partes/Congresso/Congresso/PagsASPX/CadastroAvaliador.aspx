﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CadastroAvaliador.aspx.cs" Inherits="Congresso.PagsASPX.FormAvaliador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset style="width: 313px">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1">
                            <asp:Label ID="lblPrimeiro" runat="server" Text="Primeiro Nome: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtPrimeiro" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblUltimo" runat="server" Text="Ultimo Nome: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtUltimo" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblCPF" runat="server" Text="CPF: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtCPF" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblEndereco" runat="server" Text="Endereco: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblTel" runat="server" Text="Telefone: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtTel" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblLoc" runat="server" Text="Local_trabalho: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtLoc" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblSenha" runat="server" Text="Senha: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtSenha" TextMode="Password" runat="server"></asp:TextBox>
                        </div>

                        <br />
                        <div class="col-md-12">
                            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" Height="42px" Width="127px" />
                        </div>
                        <br />
                        <div class="col-md-12">
                            <asp:Label ID="lblMsg" runat="server" Text="..."></asp:Label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <br />
        </div>
    </form>
</body>
</html>
