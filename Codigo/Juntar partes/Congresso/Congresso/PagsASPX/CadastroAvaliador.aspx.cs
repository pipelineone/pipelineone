﻿using Congresso.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Congresso.PagsASPX
{
    public partial class FormAvaliador : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            string primeiro_nome = txtPrimeiro.Text;
            string ultimo_nome = txtUltimo.Text;
            string cpf = txtCPF.Text;
            string endereco = txtEndereco.Text;
            string telefone = txtTel.Text;
            string email = txtEmail.Text;
            string local_trabalho = txtLoc.Text;
            string senha = txtSenha.Text;

            Conecta con = new Conecta();

            String cmd = @"INSERT INTO avaliador(primeiro_nome, ultimo_nome, cpf, endereco, telefone, email, local_trabalho, senha)
                    VALUES('" + primeiro_nome + "','" + ultimo_nome + "', '" + cpf + "','" + endereco + "','" + telefone + "', '" + email + "','" + local_trabalho + "', '" + senha + "')";


            //Executa o comando SQL Insert Into
            string resultado = con.ExecutaComandos(cmd);

            if (resultado.Equals("Ok"))
            {
                lblMsg.Text = "Cadastrado com Sucesso!";
            }
            else
            {
                lblMsg.Text = resultado;
            }

            Response.Redirect("ConfirmaAvaliador.aspx");
        }
    }
}