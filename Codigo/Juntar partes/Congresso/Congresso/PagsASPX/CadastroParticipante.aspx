﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CadastroParticipante.aspx.cs" Inherits="Congresso.PagsASPX.FormularioParticipante" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .row {
            width: 301px;
            height: 481px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Pagina de Cadastro</h1>

            <fieldset style="width: 313px">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1">
                            <asp:Label ID="lblPrimeiro" runat="server" Text="Primeiro Nome: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtPrimeiro" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblUltimo" runat="server" Text="Ultimo Nome: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtUltimo" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblCPF" runat="server" Text="CPF: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtCPF" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblEndereco" runat="server" Text="Endereco: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblTel" runat="server" Text="Telefone: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtTel" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblLoc" runat="server" Text="Local de Trabalho: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtLoc" runat="server"></asp:TextBox>
                        </div>
                        
                        <div class="col-md-12">
                            <asp:Label ID="lblBandeira" runat="server" Text="Cartão de Credito:"></asp:Label>
                            <br />
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="51px" ImageUrl="~/Imagens/master.png" Width="78px" OnClick="ImageButton1_Click" />
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagens/visa.png" Height="51px" Width="78px" OnClick="ImageButton2_Click"/>
                            <br />
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblNumcard" runat="server" Text="Numero do Cartão: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtNumCard" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblVencimento" runat="server" Text="Data de Vencimento: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtVenc" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="lblSenha" runat="server" Text="Senha: "></asp:Label>
                        </div>
                        <div class="col-md-11">
                            <asp:TextBox ID="txtSenha" TextMode="Password" runat="server"></asp:TextBox>
                        </div>

                        <br />
                        <div class="col-md-12">
                            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" Height="42px" Width="127px" />
                        </div>
                        <br />
                        <div class="col-md-12">
                            <asp:Label ID="lblMsg" runat="server" Text="..."></asp:Label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <br />
        </div>
    </form>
</body>
</html>
