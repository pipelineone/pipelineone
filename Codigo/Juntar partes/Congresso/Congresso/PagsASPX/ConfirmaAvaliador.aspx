﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmaAvaliador.aspx.cs" Inherits="Congresso.PagsASPX.ConfirmaAvaliador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Avaliador cadastrado com sucesso!</h1>
            <asp:HyperLink ID="hlConfirmaAvaliador" runat="server" NavigateUrl="~/PagsASPX/LoginAvaliador.aspx">Pagina de Login &gt;&gt;</asp:HyperLink>
        </div>
    </form>
</body>
</html>
