﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnviaArtigo.aspx.cs" Inherits="Congresso.PagsASPX.EnviaArtigo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:Label ID="Label1" runat="server" Text="Autor"></asp:Label>
        <br />
        <asp:TextBox ID="txtAutor" runat="server" Width="219px"></asp:TextBox>
        <br />
        <asp:Label ID="lblDestinatario" runat="server" Text="Destinatario"></asp:Label>
        <br />
        <br />
        <asp:Label ID="lblDescricao" runat="server" Text="Descrição"></asp:Label>
        <br />
        <asp:TextBox ID="txtDescricao" runat="server" Width="218px" Height="73px" TextMode="MultiLine"></asp:TextBox>
        <br />
        <asp:Label ID="lblArquivo" runat="server" Text="Arquivo"></asp:Label>
        <br />
        <asp:FileUpload ID="fuArquivo" runat="server" />
        <br />
        <br />
        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" Width="224px" OnClick="btnEnviar_Click" />
        <br />
        <asp:Label ID="lblMsg" runat="server" Text="..."></asp:Label>
        
        
        <br />
        <asp:HyperLink ID="hpPainel" runat="server" NavigateUrl="~/PagsASPX/PainelParticipante.aspx">...</asp:HyperLink>
        
    </form>
</body>
</html>
