﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginAvaliador.aspx.cs" Inherits="Congresso.PagsASPX.LoginAvaliador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>LoginAvaliador</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>Login:</td>
                    <td>
                        <asp:TextBox ID="txtCPF" runat="server" ForeColor="Gray" Width="151px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="151px"></asp:TextBox></td>
                </tr>
                
                <tr>
                    <td colspan="2" align="center">
                        <br />
                        *Entre com o CPF e senha<br />
                        <br />
                        <asp:Button ID="btnEntrar" runat="server" Text="Login" Font-Bold="True" OnClick="btnEntrar_Click" Height="32px" Width="197px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lbl1" runat="server" Text="..."></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
