﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PainelAvaliador.aspx.cs" Inherits="Congresso.PagsASPX.PainelAvaliador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="height: 387px">
    <form id="form1" runat="server">
        <div>
            <h3>Postará a nota da avaliação do artigo aqui..</h3>
            <br />
            <br />
            <asp:Label ID="lblNomeAutor" runat="server" Text="Nome do Autor"></asp:Label><br />
            <asp:TextBox ID="txtNomeAutor" runat="server" Width="159px"></asp:TextBox><br /><br />
            <asp:Label ID="lblInscricaoAutor" runat="server" Text="Inscrição do Autor"></asp:Label><br />
            <asp:TextBox ID="txtInscricaoAutor" runat="server" Width="166px"></asp:TextBox><br /><br />
            <asp:Label ID="lblFeedback" runat="server" Text="Feedback"></asp:Label><br />
            <asp:TextBox ID="txtFeedback" runat="server" Height="63px" TextMode="MultiLine"></asp:TextBox><br /><br />
            <asp:Label ID="lblNota" runat="server" Text="Nota para o artigo"></asp:Label><br />
            <asp:TextBox ID="txtNota" runat="server" TextMode="Number" Width="166px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnSalvar" runat="server" Height="38px" OnClick="btnSalvar_Click" Text="Salvar" Width="174px" />
            <br /><br />
        </div>
    </form>
</body>
</html>
