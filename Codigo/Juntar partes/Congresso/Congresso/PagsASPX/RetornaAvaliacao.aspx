﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RetornaAvaliacao.aspx.cs" Inherits="Congresso.PagsASPX.RetornaAvaliacao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 384px">
            <h1>Retorna a nota aqui</h1>
            <asp:Label ID="Label1" runat="server" Text="Autor"></asp:Label>
            <br />

            <asp:TextBox ID="TextBox1" runat="server" Width="177px"></asp:TextBox>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Iscrição"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox2" runat="server" Width="172px"></asp:TextBox>
            <br />
            <asp:Label ID="Label3" runat="server" Text="Avaliação"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox3" runat="server" Height="74px" TextMode="MultiLine"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text="Nota"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox4" runat="server" TextMode="Number"></asp:TextBox>

        </div>
    </form>
</body>
</html>
