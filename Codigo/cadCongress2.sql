create database cadCongress;

use cadCongress;

drop table usuarios;

create table usuarios (
	id_user int auto_increment not null primary key,
    nome varchar (50) not null,
    endereco varchar (100) not null,
    telefone varchar (14) not null,
    email varchar (50) not null,
    local_trabalho varchar (100) not null,
    num_card varchar (19) not null,
    data_vencimento varchar (10) not null,
    bandeira varchar (30) not null,
    arquivo varchar (100)
    );
 
 desc usuarios;
 
 select id_user from usuarios;
 
  select * from usuarios;
  
  create table cad (
	id_user int auto_increment not null primary key,
    nome varchar (50) not null,
    telefone varchar (14) not null
    );

insert into cad(nome, telefone) values("nom1","tel1");
insert into cad(nome, telefone) values("nom2","tel2");
insert into cad(nome, telefone) values("nom3","tel3");
insert into cad(nome, telefone) values("nom4","tel4");
insert into cad(nome, telefone) values("nom5","tel5");

alter table cad add email varchar(50) not null;

drop table logon;

  create table cad (
	id_user int auto_increment not null primary key,
    nome varchar (50) not null,
    telefone varchar (14) not null,
    email varchar(50) not null
    );
    
create table logon (
	id_user int auto_increment not null primary key,
    nomeUser varchar (50) not null,
    senha varchar (14) not null
    );
    
    use cadCongress;
    
insert into logon(nomeUser, senha) values("hao","050708");
insert into logon(nomeUser, senha) values("admin","admin");
insert into logon(nomeUser, senha) values("outro","outro");

desc cad;

select * from cad where id_user = 1;
select * from logon;

select * from usuarios;

insert into logon(nomeUser, senha) values('participante','participante');
insert into logon(nomeUser, senha) values('avaliador','avaliador');

select * from logon;

alter table usuarios add nomeUser varchar(50) not null;
alter table usuarios add senha varchar(50) not null;

create table usuarios (
	id_user int auto_increment not null primary key,
    nome varchar (50) not null,
    endereco varchar (100) not null,
    telefone varchar (14) not null,
    email varchar (50) not null,
    local_trabalho varchar (100) not null,
    num_card varchar (19),
    data_vencimento varchar (10),
    bandeira varchar (30),
    nomeUser varchar(50) not null,
    senha varchar(50) not null
    );

desc usuarios;

insert into usuarios(nome, endereco, telefone, email, local_trabalho, num_card, data_vencimento, bandeira, nomeUser, senha)
		values('participante','participante','participante','participante','participante','participante','000000','participante','participante','participante');

insert into usuarios(nome, endereco, telefone, email, local_trabalho, nomeUser, senha)
values('avaliador','avaliador','avaliador','avaliador','avaliador','avaliador','avaliador');

select * from usuarios;


create table artigo (
	id_arquivo int auto_increment not null primary key,
    autor varchar (50) not null,
    descricao varchar (150) not null,
    arquivo varchar(150)not null
    );






