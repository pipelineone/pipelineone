create database congresso_nassau;

use congresso_nassau;


create table participante(
	id_participante int primary key auto_increment,
    primeiro_nome varchar (50) not null,
    ultimo_nome varchar (50) not null,
    cpf varchar (11) not null,
    endereco varchar (200),
    telefone varchar (14),
    email varchar (50) not null,
    local_trabalho varchar (200),
    num_card varchar (20) not null,
    data_venc varchar (5) not null,
    senha varchar (50) not null
);
create table avaliador(
	id_avaliador int primary key auto_increment,
    primeiro_nome varchar (50) not null,
    ultimo_nome varchar (50) not null,
    cpf varchar (11) not null,
    endereco varchar (200),
    telefone varchar (14),
    email varchar (50) not null,
    local_trabalho varchar (200),
    senha varchar (50) not null
);
create table artigo (
	id_artigo int primary key auto_increment,
	autor varchar (50) not null,
    descricao varchar (300) not null,
    arquivo varchar(5000)not null
    );

create table avaliacao(
	primeiro_nome varchar (50),
	id_participante int,
    feedback varchar (400),
    nota int
);

select * from participante;
